package com.irvan.app.kataloghp.model;

import java.util.List;

/**
 * Created by irvan on 10/27/2016.
 */

public class ProductBySearch {
    private String status;
    private List<Data> data;

    public String getStatus() {
        return status;
    }

    public List<Data> getData() {
        return data;
    }

    public static class Data{
        private String title;
        private String img;
        private String slug;

        public String getTitle() {
            return title;
        }

        public String getImg() {
            return img;
        }

        public String getSlug() {
            return slug;
        }
    }
}

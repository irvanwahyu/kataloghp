package com.irvan.app.kataloghp;

import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irvan.app.kataloghp.adapter.ProductAdapter;
import com.irvan.app.kataloghp.model.AllProductByBrand;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity implements View.OnClickListener{

    LinearLayout activityProduct;
    SwipeRefreshLayout refresh;
    RecyclerView rvProduct;
    ProductAdapter productAdapter;
    String slug, title;
    AllProductByBrand allProductByBrand;
    int total_page = 0;
    int currentPage = 1;
    int page;
    TextView txtPage;
    ImageView btnPrev, btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        allProductByBrand = new AllProductByBrand();

        title = getIntent().getStringExtra(Constants.TITLE);
        slug = getIntent().getStringExtra(Constants.SLUG);

        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activityProduct = (LinearLayout)findViewById(R.id.activity_product);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh_p);
        rvProduct = (RecyclerView)findViewById(R.id.rv_product);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rvProduct.setLayoutManager(gridLayoutManager);
        rvProduct.setHasFixedSize(true);

        txtPage = (TextView)findViewById(R.id.page);
        btnPrev = (ImageView)findViewById(R.id.btn_prev);
        btnNext = (ImageView)findViewById(R.id.btn_next);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

        txtPage.setVisibility(View.INVISIBLE);
        btnPrev.setVisibility(View.INVISIBLE);
        btnNext.setVisibility(View.INVISIBLE);

        loadData(currentPage);

        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    private void loadData(final int totalPage){
        if (refresh != null)
            refresh.post(new Runnable() {
                @Override
                public void run() {
                    refresh.setRefreshing(true);
                }
            });

        API api = API.Factory.create();

        Map<String, String> data = new HashMap<>();
        data.put("view", Constants.BRAND);
        data.put("slug", slug);
        data.put("page", String.valueOf(totalPage));

        Call<AllProductByBrand> listProduct = api.getListByBrand(data);
        listProduct.enqueue(new Callback<AllProductByBrand>() {
            @Override
            public void onResponse(Call<AllProductByBrand> call, Response<AllProductByBrand> response) {
                AllProductByBrand mAllProductByBrand = response.body();
                if (mAllProductByBrand.getStatus().equals("sukses") && mAllProductByBrand.getData() != null){
                    total_page = mAllProductByBrand.getTotal_page();
                    productAdapter = new ProductAdapter(ProductActivity.this, response.body().getData());
                    rvProduct.setAdapter(productAdapter);
                    txtPage.setVisibility(View.VISIBLE);
                    txtPage.setText(String.valueOf(totalPage));
                    if (currentPage == 1){
                        btnPrev.setVisibility(View.INVISIBLE);
                    } else {
                        btnPrev.setVisibility(View.VISIBLE);
                    }
                    if (currentPage == total_page){
                        btnNext.setVisibility(View.INVISIBLE);
                    } else {
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    if (total_page == 0){
                        btnNext.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Snackbar.make(activityProduct, "Data not found", Snackbar.LENGTH_LONG).show();
                }

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<AllProductByBrand> call, Throwable t) {
                Snackbar.make(activityProduct, "Failed to connect", Snackbar.LENGTH_INDEFINITE).show();

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }
        });
    }

    private void refreshData(){
        new Handler().post(
                new Runnable() {
            @Override
            public void run() {
                loadData(currentPage);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_prev:
                page = currentPage - 1;
                currentPage = page;
                loadData(page);
                break;
            case R.id.btn_next:
                page = currentPage + 1;
                currentPage = page;
                loadData(page);
                break;
        }
    }
}

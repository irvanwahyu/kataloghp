package com.irvan.app.kataloghp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by irvan on 10/27/2016.
 */

public class DetailProduct {
    private String status;
    private String title;
    private String img;
    private Data data;

    public String getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }

    public Data getData() {
        return data;
    }

    public static class Data{
        private Network network;
        private Launch launch;
        private Body body;
        private Display display;
        private Platform platform;
        private Memory memory;
        private Camera camera;
        private Sound sound;
        private Comms comms;
        private Features features;
        private Battery battery;
        private Misc misc;
        private Test test;

        public Network getNetwork() {
            return network;
        }

        public Launch getLaunch() {
            return launch;
        }

        public Body getBody() {
            return body;
        }

        public Display getDisplay() {
            return display;
        }

        public Platform getPlatform() {
            return platform;
        }

        public Memory getMemory() {
            return memory;
        }

        public Camera getCamera() {
            return camera;
        }

        public Sound getSound() {
            return sound;
        }

        public Comms getComms() {
            return comms;
        }

        public Features getFeatures() {
            return features;
        }

        public Battery getBattery() {
            return battery;
        }

        public Misc getMisc() {
            return misc;
        }

        public Test getTest() {
            return test;
        }

        public static class Network{
            private String technology;
            @SerializedName("2g_bands")
            private String two_g_bands;
            @SerializedName("3g_bands")
            private String three_g_bands;
            @SerializedName("4g_bands")
            private String four_g_bands;
            private String speed;
            private String gprs;
            private String edge;

            public String getTechnology() {
                return technology;
            }

            public String getTwo_g_bands() {
                return two_g_bands;
            }

            public String getThree_g_bands() {
                return three_g_bands;
            }

            public String getFour_g_bands() {
                return four_g_bands;
            }

            public String getSpeed() {
                return speed;
            }

            public String getGprs() {
                return gprs;
            }

            public String getEdge() {
                return edge;
            }
        }

        public static class Launch{
            private String announced;
            private String status;

            public String getAnnounced() {
                return announced;
            }

            public String getStatus() {
                return status;
            }
        }

        public static class Body{
            private String dimensions;
            private String weight;
            private String build;
            private String sim;

            public String getDimensions() {
                return dimensions;
            }

            public String getWeight() {
                return weight;
            }

            public String getBuild() {
                return build;
            }

            public String getSim() {
                return sim;
            }
        }

        public static class Display{
            private String type;
            private String size;
            private String resolution;
            private String multitouch;
            private String protection;

            public String getType() {
                return type;
            }

            public String getSize() {
                return size;
            }

            public String getResolution() {
                return resolution;
            }

            public String getMultitouch() {
                return multitouch;
            }

            public String getProtection() {
                return protection;
            }
        }

        public static class Platform{
            private String os;
            private String chipset;
            private String cpu;
            private String gpu;

            public String getOs() {
                return os;
            }

            public String getChipset() {
                return chipset;
            }

            public String getCpu() {
                return cpu;
            }

            public String getGpu() {
                return gpu;
            }
        }

        public static class Memory{
            private String card_slot;
            private String internal;

            public String getCard_slot() {
                return card_slot;
            }

            public String getInternal() {
                return internal;
            }
        }

        public static class Camera{
            private String primary;
            private String features;
            private String video;
            private String secondary;

            public String getPrimary() {
                return primary;
            }

            public String getFeatures() {
                return features;
            }

            public String getVideo() {
                return video;
            }

            public String getSecondary() {
                return secondary;
            }
        }

        public static class Sound{
            private String alert_types;
            private String loudspeaker_;
            @SerializedName("35mm_jack_")
            private String jack;

            public String getAlert_types() {
                return alert_types;
            }

            public String getLoudspeaker_() {
                return loudspeaker_;
            }

            public String getJack() {
                return jack;
            }
        }

        public static class Comms{
            private String wlan;
            private String bluetooth;
            private String gps;
            private String nfc;
            private String radio;
            private String usb;

            public String getWlan() {
                return wlan;
            }

            public String getBluetooth() {
                return bluetooth;
            }

            public String getGps() {
                return gps;
            }

            public String getNfc() {
                return nfc;
            }

            public String getRadio() {
                return radio;
            }

            public String getUsb() {
                return usb;
            }
        }

        public static class Features{
            private String sensors;
            private String messaging;
            private String browser;
            private String java;

            public String getSensors() {
                return sensors;
            }

            public String getMessaging() {
                return messaging;
            }

            public String getBrowser() {
                return browser;
            }

            public String getJava() {
                return java;
            }
        }

        public static class Battery{
            private String _empty_;
            private String stand_by;
            private String talk_time;

            public String get_empty_() {
                return _empty_;
            }

            public String getStand_by() {
                return stand_by;
            }

            public String getTalk_time() {
                return talk_time;
            }
        }

        public static class Misc{
            private String colors;
            private String sar_eu;
            private String price_group;

            public String getColors() {
                return colors;
            }

            public String getSar_eu() {
                return sar_eu;
            }

            public String getPrice_group() {
                return price_group;
            }
        }

        public static class Test{
            private String performance;
            private String camera;
            private String loudspeaker;
            private String battery_life;
            private String _empty_;

            public String getPerformance() {
                return performance;
            }

            public String getCamera() {
                return camera;
            }

            public String getLoudspeaker() {
                return loudspeaker;
            }

            public String getBattery_life() {
                return battery_life;
            }

            public String get_empty_() {
                return _empty_;
            }
        }
    }
}

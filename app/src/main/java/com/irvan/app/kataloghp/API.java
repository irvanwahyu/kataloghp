package com.irvan.app.kataloghp;

import com.irvan.app.kataloghp.model.AllBrand;
import com.irvan.app.kataloghp.model.AllProductByBrand;
import com.irvan.app.kataloghp.model.DetailProduct;
import com.irvan.app.kataloghp.model.ProductBySearch;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by irvan on 10/27/2016.
 */

public interface API {
    @GET("gsm-arena")
    Call<AllBrand> getListBrand(@Query("view") String view);

    @GET("gsm-arena")
    Call<AllProductByBrand> getListByBrand(@QueryMap Map<String, String> data);

    @GET("gsm-arena")
    Call<ProductBySearch> getListSearchResult(@QueryMap Map<String, String> result);

    @GET("gsm-arena")
    Call<DetailProduct> getLisDetailProduct(@QueryMap Map<String, String> detail);

    class Factory{
        public static API create(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(API.class);
        }
    }
}

package com.irvan.app.kataloghp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irvan.app.kataloghp.Constants;
import com.irvan.app.kataloghp.ProductActivity;
import com.irvan.app.kataloghp.R;
import com.irvan.app.kataloghp.model.AllBrand;

import java.util.List;

/**
 * Created by irvan on 10/27/2016.
 */

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.ViewHolder> {

    private List<AllBrand.Data> listBrand;
    private Context context;

    public BrandAdapter(Context context, List<AllBrand.Data> listBrand) {
        this.context = context;
        this.listBrand = listBrand;
    }

    public Context getContext() {
        return context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtHP, txtJml;

        public ViewHolder(View itemView) {
            super(itemView);
            txtHP = (TextView)itemView.findViewById(R.id.txt_hp);
            txtJml = (TextView)itemView.findViewById(R.id.txt_jml);
        }
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_hp, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION){
                    Intent intent = new Intent(getContext(), ProductActivity.class);
                    intent.putExtra(Constants.SLUG, listBrand.get(position).getSlug());
                    intent.putExtra(Constants.TITLE, listBrand.get(position).getTitle());
                    getContext().startActivity(intent);
                }
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AllBrand.Data brand = listBrand.get(position);
        holder.txtHP.setText(brand.getTitle());
        holder.txtJml.setText(brand.getCount());
    }

    @Override
    public int getItemCount() {
        return listBrand.size();
    }

}

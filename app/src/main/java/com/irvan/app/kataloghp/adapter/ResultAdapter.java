package com.irvan.app.kataloghp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.irvan.app.kataloghp.Constants;
import com.irvan.app.kataloghp.DetailActivity;
import com.irvan.app.kataloghp.R;
import com.irvan.app.kataloghp.model.ProductBySearch;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by irvan on 10/27/2016.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {

    private List<ProductBySearch.Data> listResult;
    private Context context;

    public ResultAdapter(Context context, List<ProductBySearch.Data> listResult) {
        this.context = context;
        this.listResult = listResult;
    }

    public Context getContext() {
        return context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        CardView cvResult;
        ImageView imgResult;
        TextView txtResult;

        public ViewHolder(View itemView) {
            super(itemView);
            cvResult = (CardView)itemView.findViewById(R.id.cv_product_result);
            imgResult = (ImageView)itemView.findViewById(R.id.img_product_result);
            txtResult = (TextView)itemView.findViewById(R.id.txt_product_result);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_product_result, parent, false);
        final ViewHolder viewholder = new ViewHolder(view);

        viewholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewholder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION){
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra(Constants.SLUG, listResult.get(position).getSlug());
                    intent.putExtra(Constants.TITLE, listResult.get(position).getTitle());
                    getContext().startActivity(intent);
                }
            }
        });

        return viewholder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductBySearch.Data result = listResult.get(position);
        Picasso.with(getContext()).load(result.getImg()).into(holder.imgResult);
        holder.txtResult.setText(result.getTitle());
    }

    @Override
    public int getItemCount() {
        return listResult.size();
    }
}

package com.irvan.app.kataloghp;

/**
 * Created by irvan on 10/27/2016.
 */

public class Constants {
    public static final String BASE_URL = "http://ibacor.com/api/";
    public static final String ALL_BRANDS = "brands";
    public static final String BRAND = "brand";
    public static final String SLUG = "slug";
    public static final String TITLE = "title";
    public static final String SEARCH = "search";
    public static final String QUERY = "q";
    public static final String PRODUCT = "product";
    public static final String KEY = "bcf0e4c12c77ed003c38051592bf1663";
    public static final String TAG = "Katalog";
}

package com.irvan.app.kataloghp;

import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.irvan.app.kataloghp.adapter.ResultAdapter;
import com.irvan.app.kataloghp.model.ProductBySearch;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends AppCompatActivity {

    RelativeLayout activityResult;
    SwipeRefreshLayout refresh;
    RecyclerView rvResult;
    ResultAdapter resultAdapter;
    String query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        query = getIntent().getStringExtra(Constants.QUERY);

        getSupportActionBar().setTitle(query);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activityResult = (RelativeLayout)findViewById(R.id.activity_search_result);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh_r);
        rvResult = (RecyclerView)findViewById(R.id.rv_product_result);
        rvResult.setLayoutManager(new LinearLayoutManager(this));
        rvResult.setHasFixedSize(true);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        loadData(query);
                    }
                });
            }
        });

        loadData(query);
    }

    private void loadData(String query){
        if (refresh != null)
            refresh.post(new Runnable() {
                @Override
                public void run() {
                    refresh.setRefreshing(true);
                }
            });

        API api = API.Factory.create();
        Map<String, String> data = new HashMap<>();
        data.put("view", Constants.SEARCH);
        data.put("q", query);

        Call<ProductBySearch> listResult = api.getListSearchResult(data);
        listResult.enqueue(new Callback<ProductBySearch>() {
            @Override
            public void onResponse(Call<ProductBySearch> call, Response<ProductBySearch> response) {
                ProductBySearch productBySearch = response.body();
                if (productBySearch.getStatus().equals("sukses") && productBySearch.getData() != null){
                    resultAdapter = new ResultAdapter(SearchResultActivity.this, response.body().getData());
                    rvResult.setAdapter(resultAdapter);
                } else {
                    Snackbar.make(activityResult, "Data not found", Snackbar.LENGTH_LONG).show();
                }

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ProductBySearch> call, Throwable t) {
                Snackbar.make(activityResult, "Failed to connect", Snackbar.LENGTH_INDEFINITE).show();

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null){
                    loadData(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.irvan.app.kataloghp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.irvan.app.kataloghp.Constants;
import com.irvan.app.kataloghp.DetailActivity;
import com.irvan.app.kataloghp.R;
import com.irvan.app.kataloghp.model.AllProductByBrand;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by irvan on 10/27/2016.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<AllProductByBrand.Data> listProduct;
    private Context context;

    public ProductAdapter(Context context, List<AllProductByBrand.Data> listProduct) {
        this.context = context;
        this.listProduct = listProduct;
    }

    public Context getContext() {
        return context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        CardView cvProduct;
        ImageView imgProduct;
        TextView txtProduct;

        public ViewHolder(View itemView) {
            super(itemView);
            cvProduct = (CardView)itemView.findViewById(R.id.cv_product);
            imgProduct = (ImageView)itemView.findViewById(R.id.img_product);
            txtProduct = (TextView) itemView.findViewById(R.id.txt_product);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_product, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION){
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra(Constants.SLUG, listProduct.get(position).getSlug());
                    intent.putExtra(Constants.TITLE, listProduct.get(position).getTitle());
                    getContext().startActivity(intent);
                }
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        AllProductByBrand.Data product = listProduct.get(position);
        Picasso.with(getContext()).load(product.getImg()).into(holder.imgProduct);
        holder.txtProduct.setText(product.getTitle());
    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}

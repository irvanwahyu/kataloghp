package com.irvan.app.kataloghp;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.irvan.app.kataloghp.model.DetailProduct;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    // activity_detail
    LinearLayout activityDetail;
    TextView txtDetail;
    ImageView imgDetail;

    // layout_network
    TextView txtNetwork;
    ScrollView svNetwork;
    TextView d_technology, d_2g, d_3g, d_4g, d_speed, d_gprs, d_edge;

    // layout_launch
    TextView txtLaunch;
    ScrollView svLaunch;
    TextView d_announced, d_status;

    // layout_body
    TextView txtBody;
    ScrollView svBody;
    TextView d_dimensions, d_weight, d_sim;

    // layout_display
    TextView txtDisplay;
    ScrollView svDisplay;
    TextView d_type, d_size, d_resolution, d_multitouch, d_protection;

    // layout_platform
    TextView txtPlatform;
    ScrollView svPlatform;
    TextView d_os, d_chipset, d_cpu, d_gpu;


    DetailProduct detailProduct;
    DetailProduct.Data detailDataProduct;

    String slug, title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        title = getIntent().getStringExtra(Constants.TITLE);
        slug = getIntent().getStringExtra(Constants.SLUG);

        getSupportActionBar().setTitle("Detail Product");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        assignUIElement();
        assignEventListener();
        loadData();
    }

    private void assignUIElement(){

        // activity_detail
        activityDetail = (LinearLayout)findViewById(R.id.activity_detail);
        txtDetail = (TextView)findViewById(R.id.txt_detail);
        imgDetail = (ImageView) findViewById(R.id.img_detail);

        // layout_network
        txtNetwork = (TextView)findViewById(R.id.txtNetwork);
        svNetwork = (ScrollView)findViewById(R.id.sv_network);
        d_technology = (TextView)findViewById(R.id.d_technology);
        d_2g = (TextView)findViewById(R.id.d_2g);
        d_3g = (TextView)findViewById(R.id.d_3g);
        d_4g = (TextView)findViewById(R.id.d_4g);
        d_speed = (TextView)findViewById(R.id.d_speed);
        d_gprs = (TextView)findViewById(R.id.d_gprs);
        d_edge = (TextView)findViewById(R.id.d_edge);

        // layout_launch
        txtLaunch = (TextView)findViewById(R.id.txtLaunch);
        svLaunch = (ScrollView)findViewById(R.id.sv_launch);
        d_announced = (TextView)findViewById(R.id.d_announced);
        d_status = (TextView)findViewById(R.id.d_status);

        // layout_body
        txtBody = (TextView)findViewById(R.id.txtBody);
        svBody = (ScrollView)findViewById(R.id.sv_body);
        d_dimensions = (TextView)findViewById(R.id.d_dimensions);
        d_weight = (TextView)findViewById(R.id.d_weight);
        d_sim = (TextView)findViewById(R.id.d_sim);

        // layout_display
        txtDisplay = (TextView)findViewById(R.id.txtDisplay);
        svDisplay = (ScrollView)findViewById(R.id.sv_display);
        d_type = (TextView)findViewById(R.id.d_type);
        d_size = (TextView)findViewById(R.id.d_size);
        d_resolution = (TextView)findViewById(R.id.d_resolution);
        d_multitouch = (TextView)findViewById(R.id.d_multitouch);
        d_protection = (TextView)findViewById(R.id.d_protection);

        // layout_platform
        txtPlatform = (TextView)findViewById(R.id.txtPlatform);
        svPlatform = (ScrollView)findViewById(R.id.sv_platform);
        d_os = (TextView)findViewById(R.id.d_os);
        d_chipset = (TextView)findViewById(R.id.d_chipset);
        d_cpu = (TextView)findViewById(R.id.d_cpu);
        d_gpu = (TextView)findViewById(R.id.d_gpu);
    }

    private void assignEventListener(){
        txtNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (svNetwork.getVisibility() == View.GONE){
                    svNetwork.setVisibility(View.VISIBLE);
                } else {
                    svNetwork.setVisibility(View.GONE);
                }
            }
        });
        txtLaunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (svLaunch.getVisibility() == View.GONE){
                    svLaunch.setVisibility(View.VISIBLE);
                } else {
                    svLaunch.setVisibility(View.GONE);
                }
            }
        });
        txtBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (svBody.getVisibility() == View.GONE){
                    svBody.setVisibility(View.VISIBLE);
                } else {
                    svBody.setVisibility(View.GONE);
                }
            }
        });
        txtDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (svDisplay.getVisibility() == View.GONE){
                    svDisplay.setVisibility(View.VISIBLE);
                } else {
                    svDisplay.setVisibility(View.GONE);
                }
            }
        });
        txtPlatform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (svPlatform.getVisibility() == View.GONE){
                    svPlatform.setVisibility(View.VISIBLE);
                } else {
                    svPlatform.setVisibility(View.GONE);
                }
            }
        });
    }

    private void loadData(){

        API api = API.Factory.create();
        Map<String, String> data = new HashMap<>();
        data.put("view", Constants.PRODUCT);
        data.put("slug", slug);

        Call<DetailProduct> listDetail = api.getLisDetailProduct(data);
        listDetail.enqueue(new Callback<DetailProduct>() {
            @Override
            public void onResponse(Call<DetailProduct> call, Response<DetailProduct> response) {
                detailProduct = response.body();
                detailDataProduct = response.body().getData();

                if (detailProduct.getStatus().equals("sukses") && detailProduct.getData() != null){
                    Picasso.with(DetailActivity.this).load(detailProduct.getImg()).into(imgDetail);
                    txtDetail.setText(detailProduct.getTitle());
                    //network
                    d_technology.setText(detailDataProduct.getNetwork().getTechnology());
                    d_2g.setText(detailDataProduct.getNetwork().getTwo_g_bands());
                    d_3g.setText(detailDataProduct.getNetwork().getThree_g_bands());
                    d_4g.setText(detailDataProduct.getNetwork().getFour_g_bands());
                    d_speed.setText(detailDataProduct.getNetwork().getSpeed());
                    d_gprs.setText(detailDataProduct.getNetwork().getGprs());
                    d_edge.setText(detailDataProduct.getNetwork().getEdge());
                    //launch
                    d_announced.setText(detailDataProduct.getLaunch().getAnnounced());
                    d_status.setText(detailDataProduct.getLaunch().getStatus());
                    //body
                    d_dimensions.setText(detailDataProduct.getBody().getDimensions());
                    d_weight.setText(detailDataProduct.getBody().getWeight());
                    d_sim.setText(detailDataProduct.getBody().getSim());
                    //display
                    d_type.setText(detailDataProduct.getDisplay().getType());
                    d_size.setText(detailDataProduct.getDisplay().getSize());
                    d_resolution.setText(detailDataProduct.getDisplay().getResolution());
                    d_multitouch.setText(detailDataProduct.getDisplay().getMultitouch());
                    d_protection.setText(detailDataProduct.getDisplay().getProtection());
                    //platform
                    d_os.setText(detailDataProduct.getPlatform().getOs());
                    d_chipset.setText(detailDataProduct.getPlatform().getChipset());
                    d_cpu.setText(detailDataProduct.getPlatform().getCpu());
                    d_gpu.setText(detailDataProduct.getPlatform().getGpu());

                } else {
                    Snackbar.make(activityDetail, "Data not found", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DetailProduct> call, Throwable t) {
                Snackbar.make(activityDetail, "Failed to connect", Snackbar.LENGTH_INDEFINITE).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

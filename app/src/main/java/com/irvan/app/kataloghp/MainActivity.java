package com.irvan.app.kataloghp;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.irvan.app.kataloghp.adapter.BrandAdapter;
import com.irvan.app.kataloghp.listener.EndlessRecyclerViewScrollListener;
import com.irvan.app.kataloghp.model.AllBrand;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvHp;
    SwipeRefreshLayout refresh;
    BrandAdapter brandAdapter;
    RelativeLayout activityMain;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        activityMain = (RelativeLayout)findViewById(R.id.activity_main);
        refresh = (SwipeRefreshLayout)findViewById(R.id.refresh);
        rvHp = (RecyclerView)findViewById(R.id.rv_hp);
        rvHp.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvHp.setLayoutManager(linearLayoutManager);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });

//        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                loadNextDataFromApi(page);
//            }
//        };
//        rvHp.addOnScrollListener(scrollListener);

        loadData();
    }

    private void loadNextDataFromApi(int offset){

    }

    private void loadData(){
        if (refresh != null)
            refresh.post(new Runnable() {
                @Override
                public void run() {
                    refresh.setRefreshing(true);
                }
            });

        API api = API.Factory.create();
        Call<AllBrand> listBrand = api.getListBrand(Constants.ALL_BRANDS);
        listBrand.enqueue(new Callback<AllBrand>() {
            @Override
            public void onResponse(Call<AllBrand> call, Response<AllBrand> response) {
                AllBrand allBrand = response.body();
                if (allBrand.getStatus().equals("sukses") && allBrand.getData() != null){
                    brandAdapter = new BrandAdapter(MainActivity.this, response.body().getData());
                    rvHp.setAdapter(brandAdapter);
                } else {
                    Snackbar.make(activityMain, "Data not found", Snackbar.LENGTH_LONG).show();
                }

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<AllBrand> call, Throwable t) {
                Snackbar.make(activityMain, "Failed to connect", Snackbar.LENGTH_INDEFINITE).show();

                if (refresh != null){
                    refresh.setRefreshing(false);
                }
            }
        });
    }

    private void refreshData(){
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                loadData();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query != null){
                    Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
                    intent.putExtra(Constants.QUERY, query);
                    startActivity(intent);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private RecyclerView.OnScrollListener rvScroll = new RecyclerView.OnScrollListener(){
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
        }
    };
}

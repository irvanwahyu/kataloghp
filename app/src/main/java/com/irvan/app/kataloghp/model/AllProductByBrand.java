package com.irvan.app.kataloghp.model;

import java.util.List;

/**
 * Created by irvan on 10/27/2016.
 */

public class AllProductByBrand {
    private String status;
    private String title;
    private List<Data> data;
    private int total_page;

    public int getTotal_page() {
        return total_page;
    }

    public void setTotal_page(int total_page) {
        this.total_page = total_page;
    }

    public String getStatus() {
        return status;
    }

    public String getTitle() {
        return title;
    }

    public List<Data> getData() {
        return data;
    }

    public static class Data{
        private String title;
        private String img;
        private String slug;

        public String getTitle() {
            return title;
        }

        public String getImg() {
            return img;
        }

        public String getSlug() {
            return slug;
        }
    }
}
